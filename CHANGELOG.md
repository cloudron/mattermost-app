[0.1.0]
* Initial version

[0.2.0]
* Downgrade to 9.7.4 because 9.8.0 has broken import

[0.3.0]
* Use a more recent pgloader

[1.0.0]
* Initial stable PostgreSQL release
* Update Mattermost to 9.8.0

[1.0.1]
* Update Mattermost to 9.8.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.8.1)

[1.1.0]
* Update Mattermost to 9.9.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.9.0)

[1.1.1]
* Update Mattermost to 9.9.2
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.9.2)

[1.2.0]
* Update Mattermost to 9.10.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.10.1)

[1.2.1]
* Update Mattermost to 9.10.2
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.10.2)

[1.3.0]
* Update Mattermost to 9.11.1
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.11.1)

[1.3.1]
* Update Mattermost to 9.11.2
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v9.11.1)

[1.4.0]
* Update Matetermost to 10.0.0
* [Full changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.0.0)

[1.5.0]
* Update Matetermost to 10.1.1

[1.5.1]
* Update Matetermost to 10.1.2
[1.5.2]
* Update mattermost to 10.1.3
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.1.3)

[1.6.0]
* Update mattermost PostgreSQL to 10.2.0
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.2.0)

[1.6.1]
* Update mattermost to 10.2.1
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.2.1)

[1.7.0]
* Update mattermost to 10.3.1
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.3.1)

[1.7.1]
* Update mattermost to 10.3.2
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.3.2)

[1.8.0]
* Update mattermost to 10.4.1
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.4.1)

[1.8.1]
* Update mattermost to 10.4.2
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.4.2)

[1.8.2]
* Update mattermost to 10.4.3
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.4.3)

[1.9.0]
* Update mattermost to 10.5.1
* [Full Changelog](https://github.com/mattermost/mattermost-server/releases/tag/v10.5.1)

