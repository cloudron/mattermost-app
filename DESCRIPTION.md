### About

Mattermost is an open source, on-prem Slack-alternative.

It offers modern communication from behind your firewall, including messaging and file sharing across PCs and phones with archiving and instant search.

**All team communication in one place, searchable and accessible anywhere**

Please see the [features pages of the Mattermost website](http://www.mattermost.org/features/) for images and further description of the functionality listed below:

#### Sharing Messaging and Files

- Send messages, comments, files and images across public, private and 1-1 channels
- Personalize notifications for unreads and mentions by channel and keyword
- Use #hashtags to tag and find messages, discussions and files

#### Archiving and Search

- Import Slack user accounts and channel archives
- Search public and private channels for historical messages and comments
- View recent mentions of your name, username, nickname, and custom search terms

#### Anywhere Access

- Use Mattermost from web-enabled PCs and phones
- Attach sound, video and image files from mobile devices
- Define team-specific branding and color themes across your devices

#### Discuss

- Visit the [Mattermost Forum](http://forum.mattermost.org/) for technical questions and answers.

### Learn More

- [Product Vision and Target Audiences](http://www.mattermost.org/vision/) - What we're solving and for whom are we building
- [Mattermost Forum](http://forum.mattermost.org/) - For technical questions and answers
- [Troubleshooting Forum](https://forum.mattermost.org/t/how-to-use-the-troubleshooting-forum/150) - For reporting bugs
- [Issue Tracker](http://www.mattermost.org/filing-issues/) - For reporting bugs
- [Feature Ideas Forum](http://www.mattermost.org/feature-requests/) - For sharing ideas for future versions
- [Contribution Guidelines](https://github.com/mattermost/platform/blob/master/CONTRIBUTING.md) - For contributing code or feedback to the project

Follow us on Twitter at [@MattermostHQ](https://twitter.com/mattermosthq), or talk to the core team on our [daily builds server](https://pre-release.mattermost.com/core) via [this invite link](https://pre-release.mattermost.com/signup_user_complete/?id=rcgiyftm7jyrxnma1osd8zswby).
